# Git Basic Commands 


## Content
- [Description](#description)
- [Commands](#commands)
- [Special Points](#special-points)


## Description
this repo contains some basic git commands


## Useful References
- [Git Internals](https://www.freecodecamp.org/news/git-internals-objects-branches-create-repo/) : Deep dive of how git works under the hood
- [Git Merge Hand Book](https://www.freecodecamp.org/news/the-definitive-guide-to-git-merge/) : Definitive Guide to Merging in Git
- [Git Rebase Hand Book](https://www.freecodecamp.org/news/git-rebase-handbook/) : Definitive Guide to Rebase in Git


## Commands
1. Install git and gitk (in ubuntu)
    ```
    sudo apt-get install git gitk
    ```

1. Initialize git in a project 
    ```
    git init
    ```

1. Show the status of the git project
    ```
    git status
    ```

1. Add global git configuration 
    ```
    git config --global user.name <your-name> 
    git config --global user.email <your-email> 
    ```

1. Add a single file to git
    ```
    git add <file-name>
    ```

1. Add multiple files to git
    ```
    git add <first-file-name> <second-file-name> ... <last-file-name>
    git add .
    ```

1. Commit a file
    ```
    git commit
    ```

1. Add and commit multiple files in same time (this only works for previously added files)
    ```
    git commit -a
    ```

1. Open git visual editor (for current branch)
    ```
    gitk
    ```

1. Open Git Visual Editor (for all branches)
    ```
    gitk --all
    ```

1. Show git logs (for current branch)
    ```
    git log --stat --oneline  #(--stat, --oneline optional)
    ```

1. Show last x logs (for current branch)
    ```
    git log -nx  #(x can be 1, 2, 3 ...)
    ```

1. Show details of the commit 
    ```
    git show <commit-id>
    ```

1. Show git logs (for all branches)
    ```
    git log --stat --oneline --all  #(--stat, --oneline optional)
    ```

1. Show git tree in terminal (for current branch)
    ```
    git log --graph --oneline  #(--oneline optional)
    ```

1. Show git tree in terminal (for all branches)
    ```
    git log --graph --all --oneline  #(--oneline optional)
    ```

1. Create a new branch
    ```
    git branch <new-branch-name>
    ```
    below two commands create a new branch named <new-branch> starting at <start-point> before switching to the branch
    ```
    git checkout -b <new-branch-name> OR
    git switch -c <new-branch-name>
    ```

1. Create a new branch or reset a branch (starting at <start-point> before switching to the branch)
    ```
    git checkout -B [<new-branch-name> | <previous-branch-name>]
    ```

1. Switch between branches 
    ```
    git switch <new-branch-name> OR 
    git checkout <new-branch-name> 
    ```

1. Merge a branch to another (first move to branch that you need to merge for)
    ```
    git merge <branch-name>
    ```

1. Delete a branch (The branch must be fully merged in its upstream branch, or in HEAD if no upstream was set with --track or --set-upstream-to.)
    ```
    git branch -d <branch-name>
    ```

1. Delete a branch (shortcut for --force --delete)
    ```
    git branch -D <branch-name>
    ```

1. Create a ssh key
    ```
    ssh-keygen
    ```

1. Join Local repo to remote (remote repo in github)
    ```
    git remote add origin git@github.com:<github-username>/<github-repo-name>.git 
    ```

1. Push local content to remote 
    ```
    git push origin <local-branch-name>
    ```

1. Add upstrem reference to local branch (For every branch that is up to date or successfully pushed, add upstream (tracking) reference)
    ```
    git push origin -u <local-branch-name>
    ```

1. Clone a github repo
    ```
    git clone git@github.com:<github-username>/<github-repo-name>.git (via ssh)
    git clone https://github.com/<github-username>/<github-repo-name>.git (via https)
    ```

1. Pull remote changes to local (first checkout to branch that you want to update)
    ```
    git fetch --prune --prune-tags
    git merge

    OR 

    git pull (do top both with single command)
    ```

1. Add a git tag (in local)
    ```
    git tag <tag-name> <tag-id>

    OR 

    git tag <tag-name> (if tag for last commit of the current branch)
    ```

1. Add a git tag (to remote)
    ```
    git push origin <tag-name>
    ```

1. Remove a git tag (in local)
    ```
    git tag -d <tag-name>
    ```

1. Remove a git tag (in remote)
    ```
    git push origin -d <tag-name>
    ```

1. Go to git manual pages (ubuntu) 
    ```
    man git
    ```

1. Go to specific command help page in git (in ubuntu)
    ```
    git help <git-command-name>
    ```

1. Change the last commit message
    ```
    git commit --amend
    ```

1. Change any previous commit message(s)
    ```
    git rebase -i HEAD~<number-of-commits-want-to-list>
    ```

1. Reset the local repo commits
    ```
    git reset <commit-id> # (--hard optional, default value is --soft, when do with --hard flag also unstaged your changes)
    ```

## Special points
- Try to avoid doing reset, rebase commands after pushing to remote 
